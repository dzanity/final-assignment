package service

import (
	"fmt"
	"log"

	"rest-api-jwt/dto"
	"rest-api-jwt/entity"
	"rest-api-jwt/repository"

	"github.com/mashingan/smapping"
)

type TaskService interface {
	Insert(b dto.TaskCreateDTO) entity.Task
	Update(b dto.TaskUpdateDTO) entity.Task
	Delete(b entity.Task)
	All() []entity.Task
	FindByID(taskID uint64) entity.Task
	IsAllowedToEdit(userID string, taskID uint64) bool
}

type taskService struct {
	taskRepository repository.TaskRepository
}

func NewTaskService(taskRepo repository.TaskRepository) TaskService {
	return &taskService{
		taskRepository: taskRepo,
	}
}

func (service *taskService) Insert(b dto.TaskCreateDTO) entity.Task {
	task := entity.Task{}
	err := smapping.FillStruct(&task, smapping.MapFields(&b))
	if err != nil {
		log.Fatalf("Failed map %v: ", err)
	}
	res := service.taskRepository.InsertTask(task)
	return res
}

func (service *taskService) Update(b dto.TaskUpdateDTO) entity.Task {
	task := entity.Task{}
	err := smapping.FillStruct(&task, smapping.MapFields(&b))
	if err != nil {
		log.Fatalf("Failed map %v: ", err)
	}
	res := service.taskRepository.UpdateTask(task)
	return res
}

func (service *taskService) Delete(b entity.Task) {
	service.taskRepository.DeleteTask(b)
}

func (service *taskService) All() []entity.Task {
	return service.taskRepository.AllTask()
}

func (service *taskService) FindByID(taskID uint64) entity.Task {
	return service.taskRepository.FindTaskByID(taskID)
}

func (service *taskService) IsAllowedToEdit(userID string, taskID uint64) bool {
	b := service.taskRepository.FindTaskByID(taskID)
	id := fmt.Sprintf("%v", b.UserID)
	return userID == id
}
