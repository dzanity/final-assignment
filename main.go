package main

import (
	"net/http"
	"rest-api-jwt/config"
	"rest-api-jwt/controller"
	docs "rest-api-jwt/docs"
	"rest-api-jwt/middleware"
	"rest-api-jwt/repository"
	"rest-api-jwt/service"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	db             *gorm.DB                  = config.SetupDatabaseConnection()
	userRepository repository.UserRepository = repository.NewUserRepository(db)
	taskRepository repository.TaskRepository = repository.NewTaskRepository(db)
	jwtService     service.JWTService        = service.NewJWTService()
	userService    service.UserService       = service.NewUserService(userRepository)
	taskService    service.TaskService       = service.NewTaskService(taskRepository)
	authService    service.AuthService       = service.NewAuthService(userRepository)
	authController controller.AuthController = controller.NewAuthController(authService, jwtService)
	userController controller.UserController = controller.NewUserController(userService, jwtService)
	taskController controller.TaskController = controller.NewTaskController(taskService, jwtService)
)

// @BasePath /api

// PingExample godoc
// @Summary ping example
// @Schemes
// @Description do ping
// @Tags example
// @Accept json
// @Produce json
// @Success 200 {string} Helloworld
// @Router /example/helloworld [get]
func Helloworld(g *gin.Context) {
	g.JSON(http.StatusOK, "helloworld")
}

func main() {
	defer config.CloseDatabaseConnection(db)
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"status":      "OK",
			"message":     "Final Assignment Golang Hacktiv8",
			"api_version": "1.0.0",
			"created_by":  "Adzan Abdul Zabar",
		})
	})

	// Testing Swagger
	docs.SwaggerInfo.BasePath = "/api"
	v1 := r.Group("/api/v1")
	{
		eg := v1.Group("/example")
		{
			eg.GET("/helloworld", Helloworld)
		}
	}
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	// End testing Swagger

	authRoutes := r.Group("api/auth")
	{
		authRoutes.POST("/login", authController.Login)
		authRoutes.POST("/register", authController.Register)
	}

	userRoutes := r.Group("api/user", middleware.AuthorizeJWT(jwtService))
	{
		userRoutes.GET("/profile", userController.Profile)
		userRoutes.PUT("/profile", userController.Update)
	}

	taskRoutes := r.Group("api/tasks", middleware.AuthorizeJWT(jwtService))
	{
		taskRoutes.GET("/", taskController.All)
		taskRoutes.POST("/", taskController.Insert)
		taskRoutes.GET("/:id", taskController.FindByID)
		taskRoutes.PUT("/:id", taskController.Update)
		taskRoutes.DELETE("/:id", taskController.Delete)
	}

	// PORT := os.Getenv("DB_PORT")
	// r.Run(":" + PORT)

	r.Run()
}
