package repository

import (
	"rest-api-jwt/entity"

	"gorm.io/gorm"
)

type TaskRepository interface {
	InsertTask(b entity.Task) entity.Task
	UpdateTask(b entity.Task) entity.Task
	DeleteTask(b entity.Task)
	AllTask() []entity.Task
	FindTaskByID(TaskID uint64) entity.Task
}

type taskConnection struct {
	connection *gorm.DB
}

func NewTaskRepository(dbConn *gorm.DB) TaskRepository {
	return &taskConnection{
		connection: dbConn,
	}
}

func (db *taskConnection) InsertTask(b entity.Task) entity.Task {
	db.connection.Save(&b)
	db.connection.Preload("User").Find(&b)
	return b
}

func (db *taskConnection) UpdateTask(b entity.Task) entity.Task {
	db.connection.Save(&b)
	db.connection.Preload("User").Find(&b)
	return b
}

func (db *taskConnection) DeleteTask(b entity.Task) {
	db.connection.Delete(&b)
}

func (db *taskConnection) FindTaskByID(TaskID uint64) entity.Task {
	var task entity.Task
	db.connection.Preload("User").Find(&task, TaskID)
	return task
}

func (db *taskConnection) AllTask() []entity.Task {
	var tasks []entity.Task
	db.connection.Preload("User").Find(&tasks)
	return tasks
}
