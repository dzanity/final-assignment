# Final Assignment Hacktiv8 - Golang
## REST API TODOS DEPLOYED IN HEROKU

Adzan Abdul Zabar
dzanity@gmail.com

- Heroku: https://todos-apps-dzanity.herokuapp.com
- GitLab source code: https://gitlab.com/dzanity/final-assignment.git

# Dependencies

- "github.com/gin-gonic/gin"
- "gorm.io/gorm"
- "github.com/joho/godotenv"
- "gorm.io/driver/mysql" or "gorm.io/driver/postgres" (for testing in local, i use mysql)
- "github.com/golang-jwt/jwt"
- "github.com/mashingan/smapping"

# Running in Local

- Set .env as your environment
- Install each dependencies above
- go run main.go

# Documentation

- Check Heroku Server Alive:

<b>GET</b>
```
https://todos-apps-dzanity.herokuapp.com
```

Response (Status: 200)

```
{
    "api_version":"1.0.0",
    "created_by":"Adzan Abdul Zabar",
    "message":"Final Assignment Golang Hacktiv8",
    "status":"OK"
}
```

- Notes: I failed to install swaggo cmd at the moment, so i include "Final Assignment.postman_collection.json" in root directory.