package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"rest-api-jwt/dto"
	"rest-api-jwt/entity"
	"rest-api-jwt/helper"
	"rest-api-jwt/service"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

type TaskController interface {
	All(context *gin.Context)
	FindByID(context *gin.Context)
	Insert(context *gin.Context)
	Update(context *gin.Context)
	Delete(context *gin.Context)
}

type taskController struct {
	taskService service.TaskService
	jwtService  service.JWTService
}

func NewTaskController(taskServ service.TaskService, jwtServ service.JWTService) TaskController {
	return &taskController{
		taskService: taskServ,
		jwtService:  jwtServ,
	}
}

func (c *taskController) All(context *gin.Context) {
	var tasks []entity.Task = c.taskService.All()
	res := helper.BuildResponse(true, "OK", tasks)
	context.JSON(http.StatusOK, res)
}

func (c *taskController) FindByID(context *gin.Context) {
	id, err := strconv.ParseUint(context.Param("id"), 0, 0)
	if err != nil {
		res := helper.BuildErrorResponse("No param id was found", err.Error(), helper.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	var task entity.Task = c.taskService.FindByID(id)
	if (task == entity.Task{}) {
		res := helper.BuildErrorResponse("Data not found", "No data with given id", helper.EmptyObj{})
		context.JSON(http.StatusNotFound, res)
	} else {
		res := helper.BuildResponse(true, "OK", task)
		context.JSON(http.StatusOK, res)
	}
}

func (c *taskController) Insert(context *gin.Context) {
	var taskCreateDTO dto.TaskCreateDTO
	errDTO := context.ShouldBind(&taskCreateDTO)
	if errDTO != nil {
		res := helper.BuildErrorResponse("Failed to process request", errDTO.Error(), helper.EmptyObj{})
		context.JSON(http.StatusBadRequest, res)
	} else {
		authHeader := context.GetHeader("Authorization")
		userID := c.getUserIDByToken(authHeader)
		convertedUserID, err := strconv.ParseUint(userID, 10, 64)
		if err == nil {
			taskCreateDTO.UserID = convertedUserID
		}
		result := c.taskService.Insert(taskCreateDTO)
		response := helper.BuildResponse(true, "OK", result)
		context.JSON(http.StatusCreated, response)
	}
}

func (c *taskController) Update(context *gin.Context) {
	var taskUpdateDTO dto.TaskUpdateDTO
	errDTO := context.ShouldBind(&taskUpdateDTO)
	if errDTO != nil {
		res := helper.BuildErrorResponse("Failed to process request", errDTO.Error(), helper.EmptyObj{})
		context.JSON(http.StatusBadRequest, res)
		return
	}

	authHeader := context.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])
	if c.taskService.IsAllowedToEdit(userID, taskUpdateDTO.ID) {
		id, errID := strconv.ParseUint(userID, 10, 64)
		if errID == nil {
			taskUpdateDTO.UserID = id
		}
		result := c.taskService.Update(taskUpdateDTO)
		response := helper.BuildResponse(true, "OK", result)
		context.JSON(http.StatusOK, response)
	} else {
		response := helper.BuildErrorResponse("You dont have permission", "You are not the owner", helper.EmptyObj{})
		context.JSON(http.StatusForbidden, response)
	}
}

func (c *taskController) Delete(context *gin.Context) {
	var task entity.Task
	id, err := strconv.ParseUint(context.Param("id"), 0, 0)
	if err != nil {
		response := helper.BuildErrorResponse("Failed tou get id", "No param id were found", helper.EmptyObj{})
		context.JSON(http.StatusBadRequest, response)
	}
	task.ID = id
	authHeader := context.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])
	if c.taskService.IsAllowedToEdit(userID, task.ID) {
		c.taskService.Delete(task)
		res := helper.BuildResponse(true, "Deleted", helper.EmptyObj{})
		context.JSON(http.StatusOK, res)
	} else {
		response := helper.BuildErrorResponse("You dont have permission", "You are not the owner", helper.EmptyObj{})
		context.JSON(http.StatusForbidden, response)
	}
}

func (c *taskController) getUserIDByToken(token string) string {
	aToken, err := c.jwtService.ValidateToken(token)
	if err != nil {
		panic(err.Error())
	}
	claims := aToken.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	return id
}
